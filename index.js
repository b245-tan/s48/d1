// console.log("Hello World")

let posts = [];
// post will serve as our mock database.
	// Array of objects
	/*
		{
			id: value,
			title: value,
			body: value
		}
	*/

let count = 1;
		
	// Add post data to the mock database
	document.querySelector("#form-add-post").addEventListener("submit", (event) => {

		// preventDefault() function stops the auto reload of the webpage when submitting
		event.preventDefault()

		let newPost = {
			id: count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		}

		console.log(newPost);

		// We use push method to add the new post to the mock database
		posts.push(newPost);
		console.log(posts);

		// Auto increment id
		count++;
		showPost(posts);

		document.querySelector("#txt-title").value = ""
		document.querySelector("#txt-body").value = ""
	})


// Show posts 

	const showPost = (posts) => {
		let postEntries = ``;

		posts.forEach((posts) => {
			postEntries += `
				<div id = "post-${posts.id}">
					<h3 id= "post-title-${posts.id}">${posts.title}</h3>
					<p id = "post-body-${posts.id}">${posts.body}</p>
					<button onclick = "editPost(${posts.id})">Edit</button>
					<button onclick = "deletePost(${posts.id})" remove.this>Delete</button>
				</div>
			`
		})

		document.querySelector("#div-post-entries").innerHTML = postEntries
	}


// Edit Post
	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML
		let body = document.querySelector(`#post-body-${id}`).innerHTML

		console.log(title)
		console.log(body)

		document.querySelector("#txt-edit-id").value = id
		document.querySelector("#txt-edit-title").value = title
		document.querySelector("#txt-edit-body").value = body
	}


// Update Post
	
	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
		event.preventDefault();

		// forEach to check/find the document to be edited

		posts.forEach(post => {
			let idToBeEdited = document.querySelector("#txt-edit-id").value;

			if(post.id == idToBeEdited){
				let title = document.querySelector("#txt-edit-title").value
				let body = document.querySelector("#txt-edit-body").value

				posts[post.id-1].title = title;
				posts[post.id-1].body = body;

				alert("Edit is successful!")
				showPost(posts)


				document.querySelector("#txt-edit-id").value = ""
				document.querySelector("#txt-edit-title").value = ""
				document.querySelector("#txt-edit-body").value = ""
			}
		})
	})


// [ACTIVITY]-------------------------------------------------------------------------------
// Delete Post

	const deletePost = (id) => {
	
		posts.splice(id-1,1)


		let titleToBeRemoved = document.querySelector(`#post-title-${id}`)
		let bodyToBeRemoved = document.querySelector(`#post-body-${id}`)
		let divToBeRemoved = document.querySelector(`div#post-${id}`)


		titleToBeRemoved.remove()
		bodyToBeRemoved.remove()
		divToBeRemoved.remove()


	}